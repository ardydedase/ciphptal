<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Path to PHPTAL library, i used 1.2.0
include 'Phptal.php';


/**
* Wrapper for PHPTAL tempalte engine
*/

class Tal extends PHPTAL{


    var $cache_path = 'system/cache'; //Default cache folder
    public $template_folder = '';
    
    /**
     * This is to initialize the PHPTal Wrapper
     * 
     * @param type $template_folder 
     */
    function __construct()
    {
        //Call PHPTAL constructor (because we can)

        parent::__construct();
        

        /**
        * Use CI config to set encoding, templates and compiled templates path
        */

        $CI = &get_instance(); 

        $this->setEncoding($CI->config->item('charset'));
        
        //$this->setTemplateRepository($CI->load->_ci_view_path);
    }
    
    public function setCachePath($path)
    {        
        
        $cache_path = $path;

        $cache_path = (empty($cache_path))? $this->cache_path : $cache_path;
        
        $this->setPhpCodeDestination($cache_path);        
        
    }


    /**
    * @param string  (template name or path)
    * @param boolean (set TRUE to return page content)
    * @result mixed (depends on second parameter)
    *
    * This method returns or echoes parsed tempalte content
    */

    public function display($tpl, $return=false)
    {
        $this->setTemplate($tpl);

        if($return){

            return $this->execute();

        }

        $this->echoExecute();

    }



}