<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//Include PHPTAL translation interface
require_once 'PHPTAL/GetTextTranslator.php';


class Translator extends PHPTAL_GetTextTranslator {
    /**
     * set the language path
     *
     * @param type $lang 
     */
    public function setLanguagesPath($lang)
    {
        $this->_languages_path = $lang;
    }


}



/* End of file translator.php */
/* Location: ./system/application/libraries/translator.php */