<?php
require_once('../application/config/environment.php');

// --------------------------------------------------------------------
// END OF USER CONFIGURABLE SETTINGS.  DO NOT EDIT BELOW THIS LINE
// --------------------------------------------------------------------

/*
 * ---------------------------------------------------------------
 *  Resolve the system path for increased reliability
 * ---------------------------------------------------------------
 */

	// Set the current directory correctly for CLI requests
	if (defined('STDIN'))            
	{
		chdir(dirname(__FILE__));
	}
        
	if (realpath($system_path) !== FALSE)
	{
		$system_path = realpath($system_path).'/';
	}

	// ensure there's a trailing slash
	$system_path = rtrim($system_path, '/').'/';
        
	// Is the system path correct?
        
	if ( !is_dir($system_path))
	{
		exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
	}

/*
 * ---------------------------------------------------------------
 *  Resolve the packages path for increased reliability
 * ---------------------------------------------------------------
 */

	if (realpath($packages_path) !== FALSE)
	{
		$packages_path = realpath($packages_path).'/';
	}

	// ensure there's a trailing slash
	$packages_path = rtrim($packages_path, '/').'/';
        
	// Is the system path correct?
	if (!is_dir($packages_path))
	{
		exit("Your packages folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
	}

        
/*
 * ---------------------------------------------------------------
 *  Resolve the languages path for increased reliability
 * ---------------------------------------------------------------
 */

	if (realpath($languages_path) !== FALSE)
	{
		$languages_path = realpath($languages_path).'/';
	}

	// ensure there's a trailing slash
	$languages_path = rtrim($languages_path, '/');
        
	// Is the system path correct?
	if (!is_dir($languages_path))
	{   
            exit("Your languages folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
	}
        

/*
 * -------------------------------------------------------------------
 *  Now that we know the path, set the main path constants
 * -------------------------------------------------------------------
 */
	// The name of THIS file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

	// The PHP file extension
	define('EXT', '.php');

	// Path to the system folder
	define('BASEPATH', str_replace("\\", "/", $system_path));

	// Path to the packages folder
	define('PACKAGESPATH', str_replace("\\", "/", $packages_path));
        
	// Path to the languages folder
	define('LANGUAGESPATH', str_replace("\\", "/", $languages_path));

	// Path to the front controller (this file)
	define('FCPATH', str_replace(SELF, '', __FILE__));

	// Name of the "system folder"
	define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));

	// The path to the "application" folder
	if (is_dir($application_folder))
	{
		define('APPPATH', $application_folder.'/');
	}
	else
	{
		if ( ! is_dir(BASEPATH.$application_folder.'/'))
		{
			exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
		}

		define('APPPATH', BASEPATH.$application_folder.'/');
	}

/*
 * --------------------------------------------------------------------
 * LOAD THE BOOTSTRAP FILE
 * --------------------------------------------------------------------
 *
 * And away we go...
 *
 */
require_once BASEPATH.'core/CodeIgniter'.EXT;

/* End of file index.php */
/* Location: ./index.php */