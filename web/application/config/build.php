<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$obj =& get_instance();

$config['package_version'] = '1.1';
$config['theme_folder'] = 'default';
$config['use_cdn'] = true;
$config['cdn_url'] = 'http://corporate.cdn.mig33.com.s3.amazonaws.com/';
$config['cdn_compressed_url'] = $config['cdn_url'] . $config['package_version'] . '/resources/compressed';
$config['compressed_url'] = $obj->config->item('corporate_base_url') . $config['package_version'] . '/resources/compressed';


$config['encrypted_session_cookie'] = array(
							      'name'			=> 'eid'
							    , 'expire'			=> time() + (60*60*24*30) // 30 Days
							    , 'session_expire'	=> 60*20 // 20 Minutes
							    , 'threshold'		=> 60*15 // 15 Minutes
							    , 'domain'			=> $obj->config->item('cookie_domain')
							);
$config['session_cookie'] = array('name' => 'sid', 'expire' => time(), 'domain' => $obj->config->item('cookie_domain'));

$config['cache_path'] = realpath(APPPATH . 'cache');


/* End of file build.php */
/* Location: ./application/config/build.php */