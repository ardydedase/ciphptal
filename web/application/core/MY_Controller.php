<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Controller
 *
 * @author ardydedase
 */
class MY_Controller extends CI_Controller {

        public function __construct() {
                parent::__construct();

                // start PHPTal

                // output mode set to be HTML5 compatible
                $this->tal->setOutputMode(PHPTAL::HTML5);

                $theme_folder = $this->config->item('theme_folder') ? $this->config->item('theme_folder') : '';

                // check the settings whether we should use optimized or raw templates
                $template_path = ($this->config->item('use_optimized_templates')) ? 
                                    realpath(APPPATH) . '/optimized_templates/' . $theme_folder : 
                                    realpath(APPPATH) . '/templates/' . $theme_folder;        

                // check the settings whether we should use CDN resources or not
                $this->tal->compressed_url = ($this->config->item('use_cdn')) ? $this->config->item('cdn_compressed_url') : $this->config->item('compressed_url');

                $this->tal->setTemplateRepository($template_path);

                // we have to do initializations here because the phptal library is in a separate folder
                // set the templates folder
                //$this->tal->template_folder = TEMPLATESPATH;

                // set the cache path
                $this->tal->setCachePath($this->config->item('cache_path'));
                        
                // end PHPTal

                // get the year for the footer
                $this->tal->year = date('Y');

                $this->tal->uri_string = rtrim(uri_string(), '/');



                // whether we want to use the gzipped js and css or not
                $this->tal->css_ext = ($this->config->item('gzip_resources')) ? '.gz.css' : '.css';
                $this->tal->js_ext = ($this->config->item('gzip_resources')) ? '.gz.js' : '.js';

                $this->tal->current_domain_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';

                $this->tal->my_segment = $this->uri->segment(2);       


        }

}