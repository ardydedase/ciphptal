<?php 
function tpl_4f8c3fd2_test_tpl__qHiird8b5kEmmQNbejrQ1w(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
$ctx->setDocType('<!DOCTYPE html>',false) ;
?>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <?php /* tag "html" from line 7 */; ?>
<html class=no-js lang=en> <!--<![endif]-->
<?php /* tag "head" from line 8 */; ?>
<head>
  <?php /* tag "meta" from line 9 */; ?>
<meta charset=utf-8>

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <?php /* tag "meta" from line 13 */; ?>
<meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">

  <?php /* tag "title" from line 15 */; ?>
<title><?php echo phptal_escape($ctx->title) ?>
</title>
  <?php /* tag "meta" from line 16 */; ?>
<meta name=description content="">

  <!-- Mobile viewport optimized: h5bp.com/viewport -->
  <?php /* tag "meta" from line 19 */; ?>
<meta name=viewport content="width=device-width">

</head>
<?php /* tag "body" from line 22 */; ?>
<body>
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
  <?php /* tag "header" from line 26 */; ?>
<header>

  </header>
  <?php /* tag "div" from line 29 */; ?>
<div role=main>

  </div>
  <?php /* tag "footer" from line 32 */; ?>
<footer>

  </footer>

</body>
</html><?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /Users/ardy/oss/ciphptal/web/application/templates/default/test.tpl.html (edit that file instead) */; ?>